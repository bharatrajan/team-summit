# README #

A simple UI displaying conference session. First level classification on tabs. Second level classification on list. Session are displayed based on thier start time

### How do I get set up? ###
After cloning this project.
* `cd team-summit`
* `npm i`
* `bower i`
* `npm start`

### Libraries used ###

* Google Polymer
* Bootstrap

### Fonts ###

* Google font : Open Sans
